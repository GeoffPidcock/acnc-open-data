#########################################################################################################
#   Program Name : databasePrep.py                                                                      #
#   Program Description:                                                                                #
#   This program prepares a SQLite table containing the ACNC data about charities in Australia.         #
#                                                                                                       #
#   Comment                                         Date                  Author                        #
#   ================================                ==========            ================              #
#   Initial Version                                 03/07/2019            Geoff Pidcock                 #
#########################################################################################################

# 0. TODO:
# Logging
# Error mgmt - Asserts and Try


import pandas as pd
import re
import sqlite3

# 1. Define source data
# # Datasets are sourced from data.gov.au - https://data.gov.au/organization/acnc
# # These datasets are refreshed often - check listings for updated links and formats

urls = {}
urls['ais_2017'] = "https://data.gov.au/data/dataset/a1f8626c-fefb-4c4d-86ea-deaa04fb1f6e/resource/8d020b50-700f-4bc4-8c78-79f83d99be7a/download/datadotgov_ais17.xlsx"
urls['ais_2016'] = "https://data.gov.au/data/dataset/7e073d71-4eef-4f0c-921b-9880fb59b206/resource/b4a08924-af4f-4def-96f7-bf32ada7ee2b/download/datadotgov_ais16.xlsx"
urls['ais_2015'] = "https://data.gov.au/data/dataset/86cad799-2601-4f23-b02c-c4c0fc3b6aff/resource/569b8e48-a0ad-4008-9d95-7f91b6cfa2aa/download/datadotgov_ais15.xlsx"
urls['ais_2014'] = "https://data.gov.au/data/dataset/d7992845-5d3b-4868-b012-71f672085412/resource/4d65259d-1ccf-4c78-a223-e2bd49dc5fb1/download/datadotgov_ais14.xlsx"


# 2. Import source data

data_raw = {}
for k, v in urls.items():
    print("{} import started".format(k)) # debug
    data_raw[k] = pd.read_excel(v)
    print("{} import finished, with shape {}".format(k, data_raw[k].shape)) # debug


# 3. Prep and join data
# # Standardise case
def column_standard(data):
    data.columns = [c.replace(' ', '_') for c in data.columns]
    data.columns =  [c.lower() for c in data.columns]
    data.columns = [re.sub(r'\W+','_',c) for c in data.columns]
    return data

cols = {}
data_prep = {}
for k, v in urls.items():
    data_prep[k] = data_raw[k].copy()
    data_prep[k] = column_standard(data_prep[k])
    cols[k] = set(data_prep[k].columns.tolist())

# # Set up relabeling dict:
relabel_list_dict = [
    {'ais_2017':'adults_25_to_65',
     'ais_2016':'adults_25_to_under_65',
     'ais_2015':'adults_25_to_u60',
     'ais_2014':'adults_25_to_60'
    },
    {'ais_2017':'basic_religious_charity',
     'ais_2016':'brc',
     'ais_2015':'brc',
     'ais_2014':'brc'
    },
    {'ais_2017':'aged_care',
     'ais_2016':'aged_care_activities',
     'ais_2015':'aged_care_activities',
     'ais_2014':'aged_care_activities'
    },
    {'ais_2017':'other_activities_description',
     'ais_2016':'other_activity_description',
     'ais_2015':'other_activity_description',
     'ais_2014':'other_activity_description'
    },
    {'ais_2017':'pre_or_post_release_offenders',
     'ais_2016':'pre_or_post_release_offenders_and_families',
     'ais_2015':'pre_or_post_release_offenders_and_families',
     'ais_2014':'pre_or_post_release_offenders_and_families'
    },
    {'ais_2017':'other_recreation',
     'ais_2016':'other_recreation_and_social_club_activity',
     'ais_2015':'other_recreation_and_social_club_activity',
     'ais_2014':'other_recreation_and_social_club_activity'
    },
    {'ais_2017':'adults___65_and_over',
     'ais_2016':'adults___65_and_over',
     'ais_2015':'elderly_60_and_over',
     'ais_2014':'elderly_60_and_over'
    },
    {'ais_2017':'how_purposes_were_pursued',
     'ais_2016':'charity_activities_and_outcomes_helped_achieve_charity_purpose',
     'ais_2015':'how_purposes_were_pursued',
     'ais_2014':'how_purposes_were_pursued'
    },
    {'ais_2017':'children', # Combo column
     'ais_2016':'children', # Combo column
     'ais_2015':'children_u13',
     'ais_2014':'children'
    },
    {'ais_2017':'females',
     'ais_2016':'females',
     'ais_2015':'women',
     'ais_2014':'women'
    },
    {'ais_2017':'males',
     'ais_2016':'males',
     'ais_2015':'men',
     'ais_2014':'men'
    },
    {'ais_2017':'general_community_in_australia',
     'ais_2016':'general_community_australia',
     'ais_2015':'general_community_in_australia',
     'ais_2014':'general_community_in_australia'
    },
    {'ais_2017':'other_education',
     'ais_2016':'other_education',
     'ais_2015':'other_educations',
     'ais_2014':'other_educations'
    },
    {'ais_2017':'communities_overseas',
     'ais_2016':'overseas_communities_or_charities',
     'ais_2015':'communities_overseas',
     'ais_2014':'communities_overseas'
    },
    {'ais_2017':'will_purposes_change_in_the_next_financial_year_',
     'ais_2016':'purpose_change_in_next_fy',
     'ais_2015':'will_purposes_change_in_the_next_financial_year_',
     'ais_2014':'will_purposes_change_in_the_next_financial_year_'
    },
    {'ais_2017':'staff___full_time',
     'ais_2016':'staff_full_time',
     'ais_2015':'staff_full_time',
     'ais_2014':'staff___full_time'
    },
    {'ais_2017':'staff___part_time',
     'ais_2016':'staff_part_time',
     'ais_2015':'staff_part_time',
     'ais_2014':'staff___part_time'
    },
    {'ais_2017':'staff___casual',
     'ais_2016':'staff_casual',
     'ais_2015':'staff_casual',
     'ais_2014':'staff___casual'
    },
    {'ais_2017':'staff___volunteers',
     'ais_2016':'staff_volunteers',
     'ais_2015':'staff_volunteers',
     'ais_2014':'staff___volunteers'
    },
]

# # Combo children feature for data_prep['ais_2017'] and data_prep['ais_2016'] and
data_prep['ais_2017']['children'] = data_prep['ais_2017'].apply(
    (lambda x: 'Y' if ((x['early_childhood___under_6']=='Y') | (x['children_6_to_under_15']=='Y')) else 'N'),axis=1)
data_prep['ais_2016']['children'] = data_prep['ais_2016'].apply(
    (lambda x: 'Y' if ((x['early_childhood_under_6']=='Y') | (x['children_6___under_15']=='Y')) else 'N'),axis=1)

def relabel(df,df_name):
    relabel_dict = {}
    for item in relabel_list_dict:
        relabel_dict[item[df_name]] = item['ais_2017']
    df.rename(columns=relabel_dict,inplace=True)
    return df

cols_postlabel = {}
for k, v in data_prep.items():
    data_prep[k] = relabel(v,k)
    cols_postlabel[k] = set(data_prep[k].columns.tolist())

cols_keep = cols_postlabel['ais_2017'].intersection(cols_postlabel['ais_2016'],cols_postlabel['ais_2015'],cols_postlabel['ais_2014'])
cols_keep.add('source') # feature added below


# 4. Union and export data as SQLite3 database
data_prep_union = pd.DataFrame()
for k, v in data_prep.items():
    v['source'] = k
    data_prep_union = pd.concat([data_prep_union, v[cols_keep]])
    print("Union rows: {}".format(data_prep_union.shape[0])) # debug

conn = sqlite3.connect('./acnc.sqlite')
data_prep_union.to_sql('charities', conn, if_exists='replace', index=False)

