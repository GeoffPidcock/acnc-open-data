# Australian Charity Data

<h6>DatabasePrep.py</h6>
Python program extracting, preparing, and exporting a database of charity data from data.gov.au. Creates a consistent record across financial years 2014 through 2017.

<h6>app.py</h6>

Flask program which uses Flask-RESTPlus to produce APIs which exposes Charity Data sitting in the database.

---
Shout out to the [ACNC](https://www.acnc.gov.au/) for making this data open!