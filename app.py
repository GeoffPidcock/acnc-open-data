#########################################################################################################
#   Program Name : app.py                                                                               #
#   Program Description:                                                                                #
#   This program creates a Flask API server for querying a database of ACNC charity info                #
#                                                                                                       #
#   Comment                                         Date                  Author                        #
#   ================================                ==========            ================              #
#   Initial Version                                 03/07/2019            Geoff Pidcock                 #
#########################################################################################################

# TODO
# - Implement pagination for get all

from flask import Flask, g, jsonify, make_response
from flask_restplus import Api, Resource, fields
import sqlite3
from os import path

app = Flask(__name__)
api = Api(app, version='1.0', title='Data Service for ACNC data from FY 2014 through 2017',
          description='This is a Flask-Restplus data service that allows a client to consume APIs related to charity information, compiled by the ACNC across mutliple years.',
          )

#Database helper
ROOT = path.dirname(path.realpath(__file__))
def connect_db():
    sql = sqlite3.connect(path.join(ROOT, "acnc.sqlite"))
    sql.row_factory = sqlite3.Row
    return sql

def get_db():
    if not hasattr(g, 'sqlite_db'):
        g.sqlite_db = connect_db()
    return g.sqlite_db


@api.route('/all')
class CharityAll(Resource):
    @api.response(200, 'SUCCESSFUL: Contents successfully loaded')
    @api.response(204, 'NO CONTENT: No content in database')
    @api.doc(description='Retrieving all records from the database for all charities and all years.')
    def get(self):
        db = get_db()
        stmt_all = """
        select
            source
            , abn
            , charity_name
            , main_activity
            , how_purposes_were_pursued
            , postcode
        from
            charities
        limit 10
        """

        details_cur = db.execute(stmt_all)
        details = details_cur.fetchall()

        return_values = []

        for detail in details:
            detail_dict = {}
            detail_dict['source'] = detail['source']
            detail_dict['abn'] = detail['abn']
            detail_dict['charity_name'] = detail['charity_name']
            detail_dict['main_activity'] = detail['main_activity']
            detail_dict['how_purposes_were_pursued'] = detail['how_purposes_were_pursued']
            detail_dict['postcode'] = detail['postcode']
            return_values.append(detail_dict)

        return make_response(jsonify(return_values), 200)

@api.route('/all/<string:abn>', methods=['GET'])
class CharityABN(Resource):
    @api.response(200, 'SUCCESSFUL: Contents successfully loaded')
    @api.response(204, 'NO CONTENT: No content in database')
    @api.doc(description='Retrieving all records from the database for a given charity ABN.')
    def get(self, abn):
        db = get_db()
        stmt_one = """
        select
            source
            , abn
            , charity_name
            , main_activity
            , how_purposes_were_pursued
            , postcode
            , staff___full_time
            , donations_and_bequests
        from
            charities
        where
            abn = ? collate nocase
        """

        details_cur = db.execute(stmt_one,[abn])
        details = details_cur.fetchall()

        return_values = []

        for detail in details:
            detail_dict = {}
            detail_dict['source'] = detail['source']
            detail_dict['abn'] = detail['abn']
            detail_dict['charity_name'] = detail['charity_name']
            detail_dict['main_activity'] = detail['main_activity']
            detail_dict['how_purposes_were_pursued'] = detail['how_purposes_were_pursued']
            detail_dict['postcode'] = detail['postcode']
            detail_dict['staff___full_time'] = detail['staff___full_time']
            detail_dict['donations_and_bequests'] = detail['donations_and_bequests']
            return_values.append(detail_dict)

        return make_response(jsonify(return_values), 200)

if __name__ == '__main__':
    app.run()